#!/usr/bin/python3
#
#
# This file is part of the BHack Beacon.
#
# Author(s):
#  - Ian Firns <firnsy@gmail.com>
#
# BHack Beacon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# BHack Beacon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BHack Beacon.  If not, see <http://www.gnu.org/licenses/>.
#
#

import math
import os
import re
import sys
from PIL import Image

def convertImage(filename):
    im = Image.open(filename)
    im = im.convert("RGB")

    if im.size[1] != 4:
        sys.stderr.write("{} does not have a height of 4px: {}px".format(filename, im.size[1]))
        return

    pixels = im.load()
    prefix = re.sub(r'-([a-z])', lambda p: p.group(1).upper(), os.path.splitext(os.path.split(filename)[1])[0])

    sys.stdout.write("const uint16_t PROGMEM {0}Data[] = {{\n".format(prefix))
    sys.stdout.write("  {0:#06x},  // frame length = {0}\n".format(int(im.size[0] * im.size[1] / 4)))

    # quantise 24-bit image into 16 bits:
    # RRRRRRRRGGGGGGGGBBBBBBBB -> RRRRRGGGGGGBBBBB
    hexValues = []

    for x in range(im.size[0]):
        for y in range(im.size[1]):
            p = pixels[x, y]

            hexValues.append(((p[0] & 0b11111000) << 8) | ((p[1] & 0b11111100) << 3) | ( p[2] >> 3))

    for i in range(0, len(hexValues)):
        if (i % 8) == 0:
            sys.stdout.write("  ")

        sys.stdout.write("{0:#0{1}x}".format(hexValues[i], 6))

        if (i != 0) and (i != len(hexValues)-1) and ((i % 8) == 7):
            sys.stdout.write(",\n")

        elif (i != len(hexValues)-1):
            sys.stdout.write(", ")

    sys.stdout.write("\n};\n");

if __name__ == "__main__":
    for i, filename in enumerate(sys.argv):
        # don't convert script name
        if i == 0:
            continue

        convertImage(filename)
