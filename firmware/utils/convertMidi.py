#!/usr/bin/python3
#
#
# This file is part of the BHack Beacon.
#
# Author(s):
#  - Ian Firns <firnsy@gmail.com>
#
# BHack Beacon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# BHack Beacon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BHack Beacon.  If not, see <http://www.gnu.org/licenses/>.
#
#

import re
import struct
import sys

from pprint import pformat, pprint

DEFAULT_MIDI_HEADER_SIZE = 14

MIDI_NOTES_ARDUINO = [
    0x0008, 0x0008, 0x0009, 0x0009, 0x000a, 0x000a, 0x000b, 0x000c,
    0x000c, 0x000d, 0x000e, 0x000f, 0x0010, 0x0011, 0x0012, 0x0013,
    0x0014, 0x0015, 0x0017, 0x0018, 0x0019, 0x001b, 0x001d, 0x001e,
    0x0020, 0x0022, 0x0024, 0x0026, 0x0029, 0x002b, 0x002e, 0x0031,
    0x0033, 0x0037, 0x003a, 0x003d, 0x0041, 0x0045, 0x0049, 0x004d,
    0x0052, 0x0057, 0x005c, 0x0062, 0x0067, 0x006e, 0x0074, 0x007b,
    0x0082, 0x008a, 0x0092, 0x009b, 0x00a4, 0x00ae, 0x00b9, 0x00c4,
    0x00cf, 0x00dc, 0x00e9, 0x00f6, 0x0105, 0x0115, 0x0125, 0x0137,
    0x0149, 0x015d, 0x0171, 0x0188, 0x019f, 0x01b8, 0x01d2, 0x01ed,
    0x020b, 0x022a, 0x024b, 0x026e, 0x0293, 0x02ba, 0x02e3, 0x030f,
    0x033e, 0x0370, 0x03a4, 0x03db, 0x0416, 0x0454, 0x0496, 0x04dc,
    0x0526, 0x0574, 0x05c7, 0x061f, 0x067d, 0x06e0, 0x0748, 0x07b7,
    0x082d, 0x08a9, 0x092d, 0x09b9, 0x0a4d, 0x0ae9, 0x0b8f, 0x0c3f,
    0x0cfa, 0x0dc0, 0x0e91, 0x0f6f, 0x105a, 0x1152, 0x125a, 0x1372,
    0x149a, 0x15d3, 0x171f, 0x187f, 0x19f4, 0x1b80, 0x1d22, 0x1ede,
    0x20b4, 0x22a5, 0x24b5, 0x26e4, 0x2934, 0x2ba7, 0x2e3f, 0x30ff
]

MIDI_TYPE_NAMES = {
    0x01: "Text",
    0x02: "Copyright Notice",
    0x03: "TrackName",
    0x21: "MIDI Port/Cable",
    0x2f: "EndOfTrack",
    0x51: "SetTempo",
    0x54: "SMPTE Offset",
    0x58: "TimeSignature",
    0x59: "KeySignature",
    0x80: "NoteOff",
    0x90: "NoteOn",
    0xa0: "AfterTouch",
    0xb0: "ControlChange",
    0xc0: "ProgramChange",
    0xd0: "ChannelAfterTouch"
}

MIDI_TYPE_LENGTHS = {
    0x80: 2,
    0x90: 2,
    0xa0: 2,
    0xb0: 2,
    0xc0: 1,
    0xd0: 1
}

def read_varlen(data):
    next_byte = True
    value = 0

    while next_byte:
        chr = data.__next__()
        next_byte = (chr & 0x80) # is the hi-bit set?
        chr = chr & 0x7f         # mask out the 8th bit
        value = value << 7       # shift last value up 7 bits
        value += chr             # add new value

    return value

class FileReader(object):
    def read(self, midifile):
        pattern = self.parse_file_header(midifile)
        for track in pattern:
            self.parse_track(midifile, track)
        return pattern

    def parse_file_header(self, midifile):
        # First four bytes are MIDI header
        magic = midifile.read(4)

        if magic != b'MThd':
            raise TypeError("Bad header in MIDI file.")

        # next four bytes are header size
        # next two bytes specify the format version
        # next two bytes specify the number of tracks
        # next two bytes specify the resolution/PPQ/Parts Per Quarter
        # (in other words, how many ticks per quater note)
        data = struct.unpack(">LHHH", midifile.read(10))

        hdrsz = data[0]
        format = data[1]
        tracks = [Track() for x in range(data[2])]
        resolution = data[3]

        # XXX: the assumption is that any remaining bytes
        # in the header are padding
        if hdrsz > DEFAULT_MIDI_HEADER_SIZE:
            midifile.read(hdrsz - DEFAULT_MIDI_HEADER_SIZE)

        return Pattern(tracks=tracks, resolution=resolution, format=format)

    def parse_track_header(self, midifile):
        # First four bytes are Track header
        magic = midifile.read(4)

        if magic != b'MTrk':
            raise TypeError("Bad track header in MIDI file: %s" % (magic))

        # next four bytes are track size
        trksz = struct.unpack(">L", midifile.read(4))[0]
        return trksz

    def parse_track(self, midifile, track):
        self.running_events = {}
        self.running_status = None
        trksz = self.parse_track_header(midifile)
        trackdata = iter(midifile.read(trksz))

        while True:
            try:
                event = self.parse_midi_event(trackdata)

                track.append(event)

            except StopIteration:
                break

    def parse_midi_event(self, trackdata):
        # first datum is varlen representing delta-time
        tick = read_varlen(trackdata)

        # next byte is status message
        stsmsg = trackdata.__next__()

        # is the event a MetaEvent?
        if stsmsg == 0xff:
            cmd = trackdata.__next__()

            # 0x2f - EndOfTrack
            # 0x51 - SetTempo
            # 0x58 - TimeSignature
            # 0x59 - KeySignature

            if cmd in [0x01, 0x02, 0x03, 0x21, 0x2f, 0x51, 0x54, 0x58, 0x59]:
                sys.stderr.write("Meta Type: {}\n".format(MIDI_TYPE_NAMES[cmd]))

            else:
                sys.stderr.write("Unknown Meta MIDI Event: {}\n".format(cmd))

            datalen = read_varlen(trackdata)
            data = [trackdata.__next__() for x in range(datalen)]

            return data

        # is this event a Sysex Event?
        elif stsmsg == 0xf0:
            data = []

            while True:
                datum = trackdata.__next__()

                if datum == 0xF7:
                    break

                data.append(datum)

            return data

        # not a Meta MIDI event or a Sysex event, must be a general message
        else:
            key = stsmsg & 0xf0

            #print("{0:#04x}".format(key))

            if key not in MIDI_TYPE_LENGTHS:
                if self.running_status is None:
                    sys.stderr.write("Unknown MIDI Event: {}".format(key))
                    sys.exit(1)

                key = self.running_status & 0xf0

                data = [key, tick, stsmsg]
                data += [trackdata.__next__() for x in range(MIDI_TYPE_LENGTHS[key] - 1)]
                return data

            else:
                self.running_status = stsmsg

                data = [key, tick]
                data += [trackdata.__next__() for x in range(MIDI_TYPE_LENGTHS[key])]
                return data

        raise Warning("Unknown MIDI Event: %d" % (stsmsg))


class Pattern(list):
    def __init__(self, tracks=[], resolution=220, format=1):
        self.format = format
        self.resolution = resolution
        super(Pattern, self).__init__(tracks)

    def __repr__(self):
        return "midi.Pattern(format=%r, resolution=%r, tracks=\\\n%s)" % (self.format, self.resolution, pformat(list(self)))

    def __getitem__(self, item):
        if isinstance(item, slice):
            indices = item.indices(len(self))
            return Pattern(resolution=self.resolution, format=self.format,
                            tracks=(super(Pattern, self).__getitem__(i) for i in xrange(*indices)))
        else:
            return super(Pattern, self).__getitem__(item)

    def __getslice__(self, i, j):
        # The deprecated __getslice__ is still called when subclassing built-in types
        # for calls of the form List[i:j]
        return self.__getitem__(slice(i,j))

class Track(list):
    def __init__(self, events=[]):
        super(Track, self).__init__(events)

    def __getitem__(self, item):
        if isinstance(item, slice):
            indices = item.indices(len(self))
            return Track((super(Track, self).__getitem__(i) for i in xrange(*indices)))
        else:
            return super(Track, self).__getitem__(item)

    def __getslice__(self, i, j):
        # The deprecated __getslice__ is still called when subclassing built-in types
        # for calls of the form List[i:j]
        return self.__getitem__(slice(i,j))

    def __repr__(self):
        return "midi.Track(\\\n  %s)" % (pformat(list(self)).replace('\n', '\n  '), )



def convertMidi(filename):
    data = open(filename, 'rb')
    reader = FileReader()
    pattern = reader.read(data)

    if len(pattern) <= 1:
        return

    melody = []
    notes = {}

    ticks = 0

    sim_notes = 0

    for i in pattern[1]:
        # only care about note types
        if len(i) == 4 and i[0] in [0x80, 0x90]:
            ticks += i[1]

            if (i[0] == 0x80) or (i[0] == 0x90 and i[3] == 0):
                # we can't play simultaneous notes through the piezo
                sim_notes = max(sim_notes, len(notes))

                duration = ticks - notes[i[2]]

                melody.append([notes[i[2]], i[2], duration])

                del notes[i[2]]

            else:
                notes[i[2]] = ticks

    melody.sort(key=lambda x: x[0])

    stretch = 1.2

    print("const uint32_t PROGMEM melodyData[] = {")
    print("  {0:#010x}, // frame length = {0}".format(len(melody)))

    for i in range(0, len(melody)):
        c1 = (int(melody[i][0] * stretch) & 0xffffffff)
        c2 = ((melody[i][1] & 0x7f) << 24) | (melody[i][2] & 0xffffff)

        if (i % 4) == 0:
            sys.stdout.write("  ")

        sys.stdout.write("{0:#010x}, {1:#010x}".format(c1, c2))

        if (i != 0) and (i != len(melody)-1) and ((i % 4) == 3):
            sys.stdout.write(",\n")

        elif (i != len(melody)-1):
            sys.stdout.write(", ")

    if sim_notes > 1:
        sys.stdout.write("\n}}; // {} simultaneous notes found - sound quality will suffer\n".format(sim_notes))
    else:
        sys.stdout.write("\n};\n")

if __name__ == "__main__":
    for i, filename in enumerate(sys.argv):
        # don't convert script name
        if i == 0:
            continue

        convertMidi(filename)
