/*
**
** This file is part of the BHack Beacon.
**
** Author(s):
**  - Ian Firns <firnsy@gmail.com>
**
** BHack Beacon is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** BHack Beacon is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with BHack Beacon.  If not, see <http://www.gnu.org/licenses/>.
**
*/

#define BTN_PRESS_LONG           300
#define BTN_SEQUENCE_RESET       600
#define BTN_SEQUENCE_LENGTH_MAX  32


bool buttonPressActive = false;
bool buttonLongPressActive = false;
bool buttonSequenceActive = false;
uint32_t buttonTimeRef = 0;
uint32_t buttonSequenceIndex = 0;
uint32_t buttonSequence = 0;

bool buttonIsSequence(uint32_t sequence, uint8_t length) {
  return (buttonSequence == sequence) && (length == buttonSequenceIndex);
}

void buttonSequenceReset() {
  buttonSequence = 0;
  buttonSequenceIndex = 0;
  buttonSequenceActive = false;
}

void buttonSequenceRecord(bool longPress) {
  buttonSequence |= ((longPress ? 0x1 : 0x0) << buttonSequenceIndex);

  buttonSequenceIndex++;

  if (buttonSequenceIndex >= BTN_SEQUENCE_LENGTH_MAX) {
    buttonSequenceReset();
  }
}
