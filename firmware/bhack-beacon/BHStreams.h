/*
**
** This file is part of BHStreams.
**
** Author(s):
**  - Ian Firns <firnsy@gmail.com>
**
** Required Dependencies:
**  - MQTT (by Joel Gehwiler - v2.4.1 or later)
**
** Optional Dependencies:
**  - ArduinoJson (by Benoit Blanchon - v5.x)
**  - ArduinoOTA (by Ivan Grokhotkov/Miguel Angel Ajo - v1.0)
**
** BHStreams is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** BHStreams is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with BHStreams.  If not, see <http://www.gnu.org/licenses/>.
**
*/

#ifndef _BH_STREAMS_
#define _BH_STREAMS_

#include <MQTT.h>
#include "MQTT_conf.h"

#if defined(ESP8266)
#  include <ESP8266WiFi.h>
#elif defined(ESP32) || defined(ARDUINO_ARCH_ESP32)
#  include <WiFi.h>
#endif

#if defined(STREAMS_JSON_ENABLE)
# include <ArduinoJson.h>
#endif

#if defined(STREAMS_OTA_ENABLE)
#  if defined(ESP8266)
#    include <ESP8266mDNS.h>
#  elif defined(ESP32) || defined(ARDUINO_ARCH_ESP32)
#    include <ESPmDNS.h>
#  endif
#  include <WiFiUdp.h>
#  include <ArduinoOTA.h>
#endif

#if defined(STREAMS_DEBUG_ENABLE)
#  define DEBUG_PRINT(...) Serial.print(__VA_ARGS__)
#  define DEBUG_PRINTF(...) Serial.printf(__VA_ARGS__)
#  define DEBUG_PRINTLN(...) Serial.println(__VA_ARGS__)
#else
#  define DEBUG_PRINT(...)
#  define DEBUG_PRINTF(...)
#  define DEBUG_PRINTLN(...)
#endif

#define STREAMS_DATA_LENGTH   64
#define STREAMS_TOPIC_LENGTH  256
#define STREAMS_VALUE_LENGTH  32
#define STREAMS_CALLBACKS_MAX 32

class BHStreams;
class BHStreams_Data;

typedef void (*BHStreamsCallback)(BHStreams_Data *data);

typedef struct {
  char *streams[STREAMS_CALLBACKS_MAX] = {NULL};
  BHStreamsCallback cbs[STREAMS_CALLBACKS_MAX] = {NULL};
} BHStreamsCallbackStack;

static BHStreamsCallbackStack callback;

class BHStreams_Data {
  public:
    BHStreams_Data(char bytes[], int length) {
      this->_data_len = length;

      // copy bytes internally
      memset(this->_data, 0, STREAMS_DATA_LENGTH);
      strncpy(this->_data, bytes, length);
    }

    ~BHStreams_Data(void) {
#if defined(STREAMS_JSON_ENABLE)
      this->_json.clear();
#endif
    }

    int dataLength() {
      return this->_data_len;
    }

    // type conversion methods
    bool toBool(void) {
      if (this->_data == NULL) {
        return false;
      }

      return (toInt() > 0 || toDouble() > 0 || this->_data[0] == 't' || this->_data[0] == 'T');
    }

    char* toChar(void) {
      return this->_data;
    }

    double toDouble(void) {
      if (this->_data == NULL) {
        return 0;
      }

      return strtod(this->_data, NULL);
    }

    float toFloat(void) {
      if (this->_data == NULL) {
        return 0;
      }

      return (float)strtod(this->_data, NULL);
    }

    int toInt(void) {
      if (this->_data == NULL) {
        return 0;
      }

      return (int)strtol(this->_data, NULL, 10);
    }

#if defined(STREAMS_JSON_ENABLE)
    JsonObject& toJson(void) {
      if (this->_data == NULL) {
        return JsonObject::invalid();
      }

      JsonObject& root = this->_json.parseObject(this->_data);

      return root;
    }
#endif

    long toLong(void) {
      if (this->_data == NULL) {
        return 0;
      }

      return strtol(this->_data, NULL, 10);
    }

    uint32_t toNeoPixel(void) {
      if (this->_data == NULL || this->_data_len != 6) {
        return 0;
      }

      char rgb[9] = "0x000000";
      strncpy(&rgb[2], toChar() + 1, 6);
      return (uint32_t)strtol(rgb, NULL, 0);
    }

    int toPinLevel(void) {
      return toBool() ? HIGH : LOW;
    }

    String toString(void) {
      if (this->_data == NULL) {
        return String();
      }

      return String(this->_data);
    }

    unsigned int toUnsignedInt(void) {
      if (this->_data == NULL) {
        return 0;
      }

      return (unsigned int)strtoul(this->_data, NULL, 10);
    }

    unsigned long toUnsignedLong(void) {
      if (this->_data == NULL) {
        return 0;
      }

      return strtoul(this->_data, NULL, 10);
    }

  private:
    char _data[STREAMS_DATA_LENGTH];
    int _data_len = 0;
#if defined(STREAMS_JSON_ENABLE)
    DynamicJsonBuffer _json;
#endif
};

static void _handle(MQTTClient *client, char topic[], char bytes[], int length) {
  for (int i=0; i<STREAMS_CALLBACKS_MAX; i++) {
    if (callback.streams[i] != NULL && strncmp(callback.streams[i], topic, strlen(topic)) == 0) {
      BHStreams_Data data(bytes, length);
      callback.cbs[i](&data);
      break;
    }
  }
}

class BHStreams {
  public:
    BHStreams(void) {
  // generate unique client ID (MQTT)
#if defined(ESP8266)
      uint32_t chipId = ESP.getChipId();
      snprintf(this->_clientId, 32, "ESP8266-%04X", chipId);
#elif defined(ESP32) || defined(ARDUINO_ARCH_ESP32)
      uint64_t chipId = ESP.getEfuseMac();
      uint16_t chip = (uint16_t)(chipId >> 64);
      snprintf(this->_clientId, 32, "ESP32-%04X", chip);
#endif

#if defined(STREAMS_OTA_ENABLE)
      ArduinoOTA.setHostname(this->_clientId);
#endif
    };

    ~BHStreams(void) {
      // cleanup allocations
      if (this->_net != NULL) {
        free(this->_net);
      }
    }

    void beginOTA(const char ssid[], const char pass[]) {
      beginWiFi(ssid, pass);
      beginOTA();
      beginMQTT();
    }

    void beginWiFi(const char ssid[], const char pass[]) {
      DEBUG_PRINTF("Debug enabled.\n");

      WiFi.mode(WIFI_STA);
      WiFi.begin(ssid, pass);

      DEBUG_PRINTF("Configured connection to: %s => %s\n", ssid, pass);
    }

    void beginOTA(void) {
#if defined(STREAMS_OTA_ENABLE)
      DEBUG_PRINTF("OTA enabled.\n");

      ArduinoOTA.onStart([]() {
        String type;
        if (ArduinoOTA.getCommand() == U_FLASH) {
          type = "sketch";
        } else { // U_SPIFFS
          type = "filesystem";
        }

        // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
        Serial.println("Initiating OTA update: " + type);
      });
      ArduinoOTA.onEnd([]() {
        Serial.println("\nComplete");
      });
      ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
        Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
      });
      ArduinoOTA.onError([](ota_error_t error) {
        Serial.printf("Error[%u]: ", error);
        if (error == OTA_AUTH_ERROR) {
          Serial.println("Auth Failed");
        } else if (error == OTA_BEGIN_ERROR) {
          Serial.println("Begin Failed");
        } else if (error == OTA_CONNECT_ERROR) {
          Serial.println("Connect Failed");
        } else if (error == OTA_RECEIVE_ERROR) {
          Serial.println("Receive Failed");
        } else if (error == OTA_END_ERROR) {
          Serial.println("End Failed");
        }
      });

      ArduinoOTA.begin();
#endif
    }

    void beginMQTT(void) {

      this->_net = new WiFiClient();
      this->_mqtt.begin("ballarathackerspace.org.au", *this->_net);
      this->_mqtt.onMessageAdvanced(_handle);
    }

    void connect() {
      this->_wantConnect = true;

      this->_connect();
    }

    void disconnect() {
      this->_wantConnect = false;

      this->_mqtt.disconnect();
    }

    char* getClientId(void) {
      return this->_clientId;
    }

    bool isConnected(void) {
      return this->_mqtt.connected();
    }

    void onFeed(const char stream[], BHStreamsCallback cb) {
      int found = -1;
      char topic[STREAMS_TOPIC_LENGTH];

      snprintf(topic, STREAMS_TOPIC_LENGTH, "streams/%s", stream);
      DEBUG_PRINTF("Registering callback to streams feed: %s (%s)\n", stream, topic);

      for (int i=0; i<STREAMS_CALLBACKS_MAX && found==-1; i++) {
        if (callback.cbs[i] != NULL && strncmp(callback.streams[i], topic, strlen(topic)) == 0) {
          found = i;
        }
      }

      if (found == -1) {
       for (int i=0; i<STREAMS_CALLBACKS_MAX; i++) {
          if (callback.cbs[i] == NULL) {
            callback.cbs[i] = cb;

            char *s = new char[strlen(topic)+1];
            strcpy(s, topic);
            callback.streams[i] = s;

            this->_link(topic);
            break;
          }
        }
      } else {
        callback.cbs[found] = cb;
      }
    }

    void onRawFeed(const char stream[], BHStreamsCallback cb) {
      int found = -1;
      char topic[STREAMS_TOPIC_LENGTH];

      snprintf(topic, STREAMS_TOPIC_LENGTH, "%s", stream);
      DEBUG_PRINTF("Registering callback to raw topic feed: %s (%s)\n", stream, topic);

      for (int i=0; i<STREAMS_CALLBACKS_MAX && found==-1; i++) {
        if (callback.cbs[i] != NULL && strncmp(callback.streams[i], topic, strlen(topic)) == 0) {
          found = i;
        }
      }

      if (found == -1) {
       for (int i=0; i<STREAMS_CALLBACKS_MAX; i++) {
          if (callback.cbs[i] == NULL) {
            callback.cbs[i] = cb;

            char *s = new char[strlen(topic)+1];
            strcpy(s, topic);
            callback.streams[i] = s;

            this->_link(topic);
            break;
          }
        }
      } else {
        callback.cbs[found] = cb;
      }
    }

    void process() {
      if (this->_mqtt.connected()) {
        this->_mqtt.loop();
#if defined(STREAMS_OTA_ENABLE)
        ArduinoOTA.handle();
#endif
      } else if (WiFi.status() == WL_CONNECTED) {
        if (this->_wantConnect) {
          this->_connectMQTT();
        }
#if defined(STREAMS_OTA_ENABLE)
        ArduinoOTA.handle();
#endif
      } else if (this->_wantConnect) {
        Serial.printf("Lost connection to Streams!\n");
        this->_connect();
      }
    }

    void setClientId(const char id[]) {
      snprintf(this->_clientId, 32, "%s", id);

#if defined(STREAMS_OTA_ENABLE)
      ArduinoOTA.setHostname(this->_clientId);
#endif
    }

    void setConnectedCallback(std::function<void()> func) {
      this->_cbConnected = func;
    }

    void setNonBlocking(bool state) {
      this->_nonBlocking = state;
    }

#if defined(STREAMS_OTA_ENABLE)
    // md5 hash of the password
    void setOTAPasswordHash(const char hash[]) {
      ArduinoOTA.setPasswordHash(hash);
    }

    // default: 8266
    void setOTAPort(int port) {
      ArduinoOTA.setPort(port);
    }
#endif

    void setWatchDogInterval(unsigned int seconds) {
      this->_watchDogInterval = seconds;
    }

    // write methods
    bool write(const char stream[], BHStreams_Data data) {
      char topic[STREAMS_TOPIC_LENGTH];
      snprintf(topic, STREAMS_TOPIC_LENGTH, "streams/%s", stream);
      return this->_mqtt.publish(topic, data.toChar(), data.dataLength());
    }

    bool write(const char stream[], bool data) {
      char topic[STREAMS_TOPIC_LENGTH];
      snprintf(topic, STREAMS_TOPIC_LENGTH, "streams/%s", stream);
      return this->_mqtt.publish(topic, data ? "T" : "F", 1);
    }

    bool write(const char stream[], const char data[]) {
      char topic[STREAMS_TOPIC_LENGTH];
      snprintf(topic, STREAMS_TOPIC_LENGTH, "streams/%s", stream);
      return this->_mqtt.publish(topic, data, (int)strlen(data));
    }

    bool write(const char stream[], double data) {
      char topic[STREAMS_TOPIC_LENGTH], value[STREAMS_VALUE_LENGTH];
      snprintf(topic, STREAMS_TOPIC_LENGTH, "streams/%s", stream);
      snprintf(value, STREAMS_VALUE_LENGTH, "%f", data);
      return this->_mqtt.publish(topic, value, strlen(value));
    }

    bool write(const char stream[], float data) {
      char topic[STREAMS_TOPIC_LENGTH], value[STREAMS_VALUE_LENGTH];
      snprintf(topic, STREAMS_TOPIC_LENGTH, "streams/%s", stream);
      snprintf(value, STREAMS_VALUE_LENGTH, "%f", data);
      return this->_mqtt.publish(topic, value, strlen(value));
    }

    bool write(const char stream[], int data) {
      char topic[STREAMS_TOPIC_LENGTH], value[STREAMS_VALUE_LENGTH];
      snprintf(topic, STREAMS_TOPIC_LENGTH, "streams/%s", stream);
      snprintf(value, STREAMS_VALUE_LENGTH, "%d", data);
      return this->_mqtt.publish(topic, value, strlen(value));
    }

    bool write(const char stream[], long data) {
      char topic[STREAMS_TOPIC_LENGTH], value[STREAMS_VALUE_LENGTH];
      snprintf(topic, STREAMS_TOPIC_LENGTH, "streams/%s", stream);
      snprintf(value, STREAMS_VALUE_LENGTH, "%l", data);
      return this->_mqtt.publish(topic, value, strlen(value));
    }

    bool write(const char stream[], String data) {
      char topic[STREAMS_TOPIC_LENGTH], value[data.length()];
      snprintf(topic, STREAMS_TOPIC_LENGTH, "streams/%s", stream);
      data.toCharArray(value, data.length());
      return this->_mqtt.publish(topic, value, data.length());
    }

    bool write(const char stream[], unsigned int data) {
      char topic[STREAMS_TOPIC_LENGTH], value[STREAMS_VALUE_LENGTH];
      snprintf(topic, STREAMS_TOPIC_LENGTH, "streams/%s", stream);
      snprintf(value, STREAMS_VALUE_LENGTH, "%u", data);
      return this->_mqtt.publish(topic, value, strlen(value));
    }

    bool write(const char stream[], unsigned long data) {
      char topic[STREAMS_TOPIC_LENGTH], value[STREAMS_VALUE_LENGTH];
      snprintf(topic, STREAMS_TOPIC_LENGTH, "streams/%s", stream);
      snprintf(value, STREAMS_VALUE_LENGTH, "%ul", data);
      return this->_mqtt.publish(topic, value, strlen(value));
    }

  private:
    Client *_net;
    MQTTClient  _mqtt;

    char _clientId[32];
    bool _wantConnect;
    bool _connectingMQTT = false;
    bool _nonBlocking = false;
    unsigned int _watchDogInterval = 0;

    // callbacks
    std::function<void()> _cbConnected;

    void _connect(void) {
      _connectWiFi();
      _connectMQTT();
    }

    void _connectMQTT(void) {
      if (!this->_connectingMQTT) {
        Serial.printf("Connecting to MQTT/Streams (%s) ... \n", this->_clientId);
        this->_connectingMQTT = true;
      }

      if (this->_nonBlocking) {
        DEBUG_PRINTF("NON BLOCKING.\n");
        this->_mqtt.connect(this->_clientId, USERNAME, PASSWORD);
      } else {
        while (!this->_mqtt.connect(this->_clientId, USERNAME, PASSWORD)) {
          DEBUG_PRINTF(".");
          delay(1000);
        }
      }

      if (this->_mqtt.connected()) {
        this->_connectingMQTT = false;

        Serial.printf("MQTT/Streams connected!\n");

        // re-link any feeds
        for (int i=0; i<STREAMS_CALLBACKS_MAX; i++) {
          if (callback.streams[i] != NULL) {
            this->_link(callback.streams[i]);
          }
        }

        if (this->_cbConnected != NULL) {
          this->_cbConnected();
        }
      }
    }

    void _connectWiFi(void) {
      unsigned int count = 0;

      if (WiFi.status() != WL_CONNECTED) {
        Serial.printf("Connecting to WiFi (%s) ... \n", this->_clientId);

        while (WiFi.status() != WL_CONNECTED) {
          if (this->_watchDogInterval > 0 && ++count > this->_watchDogInterval) {
            DEBUG_PRINTF("\nWatchdog expired (%d seconds).\nNo connection established, restarting!\n", this->_watchDogInterval);
            ESP.restart();
          }

          DEBUG_PRINTF(".");
          delay(1000);
        }

        Serial.printf("WiFi connected!\n");
      } else {
        Serial.printf("WiFi already established!\n");
      }
    }

    void _link(const char stream[]) {
      if (this->isConnected()) {
        DEBUG_PRINTF("Subscribing stream: %s\n", stream);
        this->_mqtt.subscribe(stream);
      }
    }
};

#endif
