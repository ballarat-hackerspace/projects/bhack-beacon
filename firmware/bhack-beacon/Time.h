/*
**
** This file is part of the BHack Beacon.
**
** Author(s):
**  - Ian Firns <firnsy@gmail.com>
**
** BHack Beacon is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** BHack Beacon is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with BHack Beacon.  If not, see <http://www.gnu.org/licenses/>.
**
*/

#include <time.h>

uint32_t timeEpochLastRef = 0;
uint32_t timeEpochLastSync = 0;
float timeSkew = 0;

struct tm * timeNow(void) {
  if (timeEpochLastRef == 0) {
    return NULL;
  }

  uint32_t now = millis();
  time_t adjusted = timeEpochLastRef + (uint32_t)((float)(now - timeEpochLastSync) / 1000);
  return gmtime(&adjusted);
}

bool timeSynced(void) {
  return timeEpochLastRef > 0;
}

void timeSync(uint32_t ref) {
  uint32_t now = millis();
  float refDiff = ref - timeEpochLastRef;     // unit: s
  float fetchDiff = now - timeEpochLastSync; // unit: ms

  timeSkew = refDiff / (fetchDiff / 1000);
  timeEpochLastRef = ref;
  timeEpochLastSync = now;
}
