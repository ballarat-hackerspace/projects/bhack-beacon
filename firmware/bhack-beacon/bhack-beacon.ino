/*
**
** This file is part of the BHack Beacon.
**
** Author(s):
**  - Ian Firns <firnsy@gmail.com>
**
** BHack Beacon is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** BHack Beacon is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with BHack Beacon.  If not, see <http://www.gnu.org/licenses/>.
**
*/

#define STREAMS_DEBUG_ENABLE
#define STREAMS_JSON_ENABLE

#include <Adafruit_NeoPixel.h>

#include "WiFiManager.h" // using local copy of dev branch @ git5557769
#include "BHStreams.h"
#include "Animations.h"  // LED animations and sequences
#include "Buttons.h"     // Button sequences and dedection
#include "Sounds.h"      // sound sequences and pitches
#include "Time.h"

#define BUTTON  2
#define PIXELS 13
#define PIEZO  14
#define F_RST  15


#define IN_ANIMATION -1
#define LIGHT_OFF 0
#define HS_OPEN 1
#define HS_CLOSED 2
#define HS_XMAS 3

int light_mode = 0;
int previous_light_mode = 0;

Adafruit_NeoPixel strip = Adafruit_NeoPixel(4, PIXELS, NEO_GRB + NEO_KHZ800);
BHStreams streams;
WiFiManager wifiManager;

void handleConnected(void) {
  selectAnimation(0, 5);
}

void handleAPMode(WiFiManager *wm) {
  setPixels(0, 64, 0);
}

void setup() {
  Serial.begin(115200);

  pinMode(BUTTON, INPUT);
  pinMode(PIXELS, OUTPUT);
  pinMode(PIEZO, OUTPUT);
  pinMode(F_RST, INPUT);

  setPixels(64, 0, 0);

  wifiManager.setAPCallback(handleAPMode);
  wifiManager.autoConnect(streams.getClientId(), "beacon01");

  streams.beginMQTT();
  streams.onRawFeed("internal/beacon", handleInternalBeaconFeed);
  streams.onFeed("beacon", handlePublicBeaconFeed);

  streams.setConnectedCallback(handleConnected);
  streams.connect();
}

void loop() {
  streams.process();

  uint32_t now = millis();

  handlePresses(now);
  handleAnimations(now);
  handleSounds(now);
  handleLight();
}

void handleLight(){

  if (light_mode == IN_ANIMATION){
    return;
  }

  uint16_t red = 0;
  uint16_t green = 0;
  uint16_t blue = 0;

  switch(light_mode){
    case LIGHT_OFF:
      strip.clear();
      return;
    case HS_OPEN:
      green = 80;
      break;
    case HS_CLOSED:
      red = 80;
      break;
    case HS_XMAS:
      blue = 80;
  }
  for (int i=0; i<4; i++){
      strip.setPixelColor(i, red, green, blue);
  }
  strip.show();
}

void handleInternalBeaconFeed(BHStreams_Data *data) {
  Serial.printf("Internal beacon received: %s\n", data->toChar());

  JsonObject& root = data->toJson();
  if (root.success()) {
    if (root["ts"].success()) {
      timeSync(root["ts"]);

      Serial.printf(" - TS: %d (skew %.2fs)\n", timeEpochLastRef, timeSkew);
    }

    // handle animation requests
    if (root["a"]["s"].success() && root["a"]["l"].success()) {
      Serial.printf(" - Updating animation ...\n");
      previous_light_mode = light_mode;
      selectAnimation(root["a"]["s"], root["a"]["l"]);
    }

    // handle melody requests
    if (root["m"]["s"].success() && root["m"]["l"].success()) {
      Serial.printf(" - Updating melody ...\n");
      selectMelody(root["m"]["s"], root["m"]["l"]);
    }
  }
}

void handlePublicBeaconFeed(BHStreams_Data *data) {
  Serial.printf("Public beacon received: %s\n", data->toChar());

  if (strncmp(data->toChar(), "SOS!", 4) == 0) {
    previous_light_mode = LIGHT_OFF;
    selectAnimation(9, 10);
  } else if (strncmp(data->toChar(), "HS is open!", 11) == 0) {
    previous_light_mode = HS_OPEN;
    selectAnimation(10, 10);
  } else if (strncmp(data->toChar(), "HS is closed!", 13) == 0) {
    previous_light_mode = HS_CLOSED;
    selectAnimation(6, 10);
  } else if (strncmp(data->toChar(), "Merry Xmas!", 11) == 0) {
    previous_light_mode = HS_XMAS;
    selectAnimation(12, 10);
    selectMelody(2, 1);
  }
}


void handleAnimations(uint32_t timeRef) {
  if (animationLoop != 0 && timeRef > animationTimeRef) {
    strip.clear();
    light_mode = IN_ANIMATION;
    // animations are loosely defined by colour sequences and timing
    int offset = animationIndex * 4;

    for (uint8_t i=0; i<4; i++) {
      uint16_t rgb = pgm_read_word(animationActive + offset + i);

      strip.setPixelColor(i,
        pgm_read_byte(&gamma5[rgb >> 11]),
        pgm_read_byte(&gamma6[(rgb >> 5) & 0x3f]),
        pgm_read_byte(&gamma5[rgb & 0x1f])
      );
    }

    strip.show();

    animationIndex++;
    animationTimeRef = timeRef + 40; // 40 ms per frame (25FPS)

    // process animation loops
    if (animationIndex >= animationLength) {
      animationIndex = 0;

      if (animationLoop >= 1) {
        animationLoop--;
      }
    }
  } else if (timeRef > animationTimeRef) {
    light_mode = previous_light_mode;
    strip.clear();
    strip.show();
  }
}

void handlePresses(uint32_t timeRef) {
  bool pressed = !digitalRead(BUTTON); // inverted

  if (pressed) {

    // Any press turns off the light
    light_mode = LIGHT_OFF;
    previous_light_mode = LIGHT_OFF;

    if (!buttonPressActive) {
      buttonPressActive = true;
      buttonTimeRef = timeRef;
    }

    if (timeRef - buttonTimeRef > BTN_PRESS_LONG) {
      buttonLongPressActive = true;
    }

    if (!buttonSequenceActive) {
      buttonSequenceActive = true;
    }
  } else {
    if (buttonPressActive) {
      buttonSequenceRecord(buttonLongPressActive);

      Serial.printf("BTN press detected: %s\n", buttonLongPressActive ? "L" : "S");

      buttonTimeRef = timeRef;
      buttonLongPressActive = buttonPressActive = false;
    }

    if (buttonSequenceActive) {
      if (timeRef > buttonTimeRef + BTN_SEQUENCE_RESET) {
        // check sequence
        Serial.printf("BTN sequence detected: ");

        uint32_t s = buttonSequence;
        for (int i=0; i<buttonSequenceIndex; i++, s >>= 1) {
          Serial.printf("%s", s & 0x1 ? "L" : "S");
        }

        Serial.printf(" -> (0x%08x, %d)\n", buttonSequence, buttonSequenceIndex);

        if (buttonIsSequence(0x0, 1)) {         // single short
          previous_light_mode = light_mode;
          selectAnimation(4, 1);
        } else if (buttonIsSequence(0x1, 1)) {  // single long
          previous_light_mode = light_mode;
          selectAnimation(6, 1);
        } else if (buttonIsSequence(0x0, 5)) {  // prime shorts
          previous_light_mode = light_mode;
          selectAnimation(5, 5);
        } else if (buttonIsSequence(0x1f, 5)) { // prime longs
          previous_light_mode = light_mode;
          selectAnimation(7, 5);
        } else if (buttonIsSequence(0x38, 9)) { // S.O.S.
          previous_light_mode = light_mode;
          selectAnimation(9, 10);
          streams.write("beacon", "SOS!");
        }

        buttonSequenceReset();
      }
    }
  }
}

void handleSounds(uint32_t timeRef) {
  if (melodyLoop != 0 && (timeRef > melodyTimeRef)) {
    while (timeRef > melodyTimeRef) {
      uint32_t offset = melodyIndex << 1;
      uint32_t t = pgm_read_dword(melodyActive + offset);
      uint32_t nd = pgm_read_dword(melodyActive + offset + 1);
      uint8_t n = (nd >> 24);

     tone(PIEZO, pgm_read_word(&midiNotes[n]), nd & 0xffffff);

      melodyIndex++;

      // process melody loops
      if (melodyIndex >= melodyLength) {
        melodyIndex = 0;
        melodyTimeBase = melodyTimeRef = timeRef + (nd & 0xffffff) + 1000;

        if (melodyLoop >= 1) {
          melodyLoop--;
        }
      } else {
        if (melodyTimeBase == 0) {
          melodyTimeBase = timeRef;
        }

        melodyTimeRef = melodyTimeBase + pgm_read_dword(melodyActive + offset + 2);
      }
    }
  } else if (timeRef > melodyTimeRef) {
    digitalWrite(PIEZO, LOW);
  }
}

void setPixels(uint8_t red, uint8_t green, uint8_t blue) {
  for (uint8_t i=0; i<8; i++) {
    strip.setPixelColor(i, strip.Color(red, green, blue));
  }

  strip.show();
}
